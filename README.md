# oooztk

1. Установить зависимости `go mod download`
2. БД в докере поднимается через `docker compose up` (добавить еще флаг -d, чтобы запустить как демона)
3. Применить миграции. Использовал пакет `https://github.com/golang-migrate/migrate`. Через CLI будет что-то вроде `migrate -path=migrations -database mysql://root:root@(127.0.0.1:3306)/oooztk up`
4. Сделать копию/переименовать файл конфига `.env` и отредактировать его.

## Тестирование на нагрузку
Использовал Apache Bench (ab) для 5000 запросов и 100 потоков
При запуске с подключенной БД и файлом, который отдается в боди POST'a
```
{
    "voteId": "00000000-0000-0000-0000-000000000001",
    "votingId": "00000000-0000-0000-0000-000000000001",
    "optionId": "00000000-0000-0000-0000-000000000001"
}
```
показал результаты
```
ab -p post1.json -T application/json -c 100 -n 5000 http://localhost:3000/voting
This is ApacheBench, Version 2.3 <$Revision: 1879490 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 500 requests
Completed 1000 requests
Completed 1500 requests
Completed 2000 requests
Completed 2500 requests
Completed 3000 requests
Completed 3500 requests
Completed 4000 requests
Completed 4500 requests
Completed 5000 requests
Finished 5000 requests


Server Software:        
Server Hostname:        localhost
Server Port:            3000

Document Path:          /voting
Document Length:        15 bytes

Concurrency Level:      100
Time taken for tests:   0.246 seconds
Complete requests:      5000
Failed requests:        0
Total transferred:      710000 bytes
Total body sent:        1555000
HTML transferred:       75000 bytes
Requests per second:    20337.44 [#/sec] (mean)
Time per request:       4.917 [ms] (mean)
Time per request:       0.049 [ms] (mean, across all concurrent requests)
Transfer rate:          2820.23 [Kbytes/sec] received
                        6176.70 kb/s sent
                        8996.93 kb/s total

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    2   0.4      2       4
Processing:     1    3   0.6      2       7
Waiting:        0    2   0.5      2       6
Total:          3    5   0.5      5      10
WARNING: The median and mean for the processing time are not within a normal deviation
        These results are probably not that reliable.

Percentage of the requests served within a certain time (ms)
  50%      5
  66%      5
  75%      5
  80%      5
  90%      5
  95%      6
  98%      6
  99%      6
 100%     10 (longest request)
```