package services

import (
	"os"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/indikator/oooztk/internal/api/models"
)

func SendToGamma(req models.GammaRequest) {
	agent := fiber.Post(os.Getenv("GAMMA_ENDPOINT"))
	agent.Reuse()
	agent.JSON(req).Bytes()
}
