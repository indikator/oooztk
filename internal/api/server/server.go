package server

import (
	"log"
	"os"
	"os/signal"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/indikator/oooztk/internal/api/database"
	"gitlab.com/indikator/oooztk/internal/api/models"
)

func RunServer() {
	app := fiber.New()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		log.Println("Gracefully shutting down...")
		err := app.Shutdown()
		if err != nil {
			log.Fatalf("Error while shutting down: %s\n", err.Error())
		}
	}()

	initRoutes(app)

	go listenForChanges()

	if err := app.Listen(os.Getenv("BETA_HOST") + ":" + os.Getenv("BETA_PORT")); err != nil {
		log.Panic(err)
	}

	models.GetProcessor().CloseChannels()
	close(chSendToGamma)
	database.CloseDB()
}

func initRoutes(app *fiber.App) {
	app.Get("/health", func(c *fiber.Ctx) error {
		return c.Status(fiber.StatusOK).SendString("OK")
	})

	app.Post("/voting", func(c *fiber.Ctx) error {
		var alfaRequest models.AlfaRequest
		err := c.BodyParser(&alfaRequest)
		if err != nil {
			log.Printf("error while parsing request: %s\n", err.Error())
			return c.SendStatus(fiber.StatusBadRequest)
		}

		go alfaRequest.Proceed()

		return c.Status(fiber.StatusOK).JSON(fiber.Map{"result": "ok"})
	})
}
