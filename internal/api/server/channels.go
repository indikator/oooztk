package server

import (
	"github.com/google/uuid"
	"gitlab.com/indikator/oooztk/internal/api/models"
	"gitlab.com/indikator/oooztk/internal/api/services"
)

var chSendToGamma chan uuid.UUID

func listenForChanges() {
	chSendToGamma = make(chan uuid.UUID, 10)
	models.GetProcessor().SetChannel(chSendToGamma)
	for v := range chSendToGamma {
		services.SendToGamma(models.GetProcessor().AlfaUUIDToGamma(v))
	}
}
