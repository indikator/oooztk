package models

import "github.com/google/uuid"

type OptionCount struct {
	OptionID uuid.UUID `json:"optionId"`
	Count    int       `json:"count"`
}
