package models

import "github.com/google/uuid"

type GammaRequest struct {
	VotingID uuid.UUID     `json:"votingId"`
	Results  []OptionCount `json:"results"`
}
