package models

import "sync"

type Count struct {
	mutex    sync.Mutex
	OldValue int
	Value    int
}

func NewCount() *Count {
	return &Count{}
}

func (c *Count) Add() (int, int) {
	c.mutex.Lock()
	c.OldValue = c.Value
	c.Value++
	c.mutex.Unlock()
	return c.OldValue, c.Value
}
