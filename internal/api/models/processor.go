package models

import (
	"sync"

	"github.com/google/uuid"
)

var proc *Processor

type Processor struct {
	m     map[uuid.UUID]*Options
	mutex sync.Mutex
	ch    chan uuid.UUID
}

func newProcessor() *Processor {
	return &Processor{m: map[uuid.UUID]*Options{}}
}

func GetProcessor() *Processor {
	if proc == nil {
		proc = newProcessor()
	}
	return proc
}

func (p *Processor) SetChannel(ch chan uuid.UUID) {
	p.ch = ch
}

func (p *Processor) GetChannel() chan uuid.UUID {
	return p.ch
}

func (p *Processor) AddAlfa(req AlfaRequest) {
	votingId := req.VotingID
	p.mutex.Lock()
	defer p.mutex.Unlock()
	if _, ok := p.m[votingId]; !ok {
		p.m[votingId] = NewOptions(votingId)
	}

	p.m[votingId].Add(req.OptionID)
}

func (p *Processor) AlfaUUIDToGamma(uuid uuid.UUID) GammaRequest {
	return GammaRequest{VotingID: uuid, Results: p.m[uuid].GammaResult()}
}

func (p *Processor) CloseChannels() {
	p.mutex.Lock()
	defer p.mutex.Unlock()
	for _, v := range p.m {
		close(v.ChanGamma)
	}
}
