package models

import (
	"sync"
	"time"

	"github.com/google/uuid"
)

type Options struct {
	mutex     sync.Mutex
	VotingID  uuid.UUID
	OldSum    int
	Sum       int
	Results   map[uuid.UUID]*Count
	ChanGamma chan uuid.UUID
}

func NewOptions(id uuid.UUID) *Options {
	ch := make(chan uuid.UUID, 1)
	go prepareSendToGamma(ch)
	return &Options{VotingID: id, Results: map[uuid.UUID]*Count{}, ChanGamma: ch}
}

func prepareSendToGamma(ch <-chan uuid.UUID) {
	for uuid := range ch {
		GetProcessor().GetChannel() <- uuid
		time.Sleep(5000 * time.Millisecond)
	}
}

func (o *Options) Add(optionId uuid.UUID) {
	o.mutex.Lock()
	defer o.mutex.Unlock()
	if _, ok := o.Results[optionId]; !ok {
		o.Results[optionId] = NewCount()
	}

	o.OldSum = o.Sum
	o.Sum++

	oldValue, newValue := o.Results[optionId].Add()
	if len(o.ChanGamma) == 0 && (o.OldSum == 0 || (oldValue*100/o.OldSum != newValue*100/o.Sum)) {
		o.ChanGamma <- o.VotingID
	}
}

func (o *Options) GammaResult() []OptionCount {
	result := make([]OptionCount, len(o.Results))
	o.mutex.Lock()
	defer o.mutex.Unlock()
	var i int
	for optionId, count := range o.Results {
		result[i] = OptionCount{OptionID: optionId, Count: count.Value}
		i++
	}
	return result
}
