package models

import (
	"github.com/google/uuid"
	"gitlab.com/indikator/oooztk/internal/api/database"
)

type AlfaRequest struct {
	VoteID   uuid.UUID `json:"voteId"`
	VotingID uuid.UUID `json:"votingId"`
	OptionID uuid.UUID `json:"optionId"`
}

func (a *AlfaRequest) Proceed() {
	GetProcessor().AddAlfa(*a)
	database.InsertVote(a.VoteID, a.VotingID, a.OptionID)
}
