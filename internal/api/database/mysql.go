package database

import (
	"context"
	"database/sql"
	"log"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/google/uuid"
)

var db *sql.DB

func InitDB() {
	var err error
	db, err = sql.Open("mysql", os.Getenv("MYSQL_CONNECTION"))
	if err != nil {
		log.Fatal("error while opening DB connection:", err.Error())
	}
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)
}

func CloseDB() {
	db.Close()
}

func InsertVote(voteId, votingId, optionId uuid.UUID) {
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	if err != nil {
		log.Fatal("error occurs while getting connection from the pool: ", err.Error())
	}

	sql := "INSERT INTO votes(vote_id, voting_id, option_id) VALUES (?, ?, ?)"
	_, err = conn.ExecContext(ctx, sql, voteId, votingId, optionId)

	if err != nil {
		log.Fatal("error occurs during exec query: ", err.Error())
	}

	err = conn.Close()
	if err != nil {
		log.Fatal("error occurs while return connection back to the pool: ", err.Error())
	}
}
