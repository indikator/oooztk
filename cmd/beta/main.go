package main

import (
	"gitlab.com/indikator/oooztk/internal/api/database"
	"gitlab.com/indikator/oooztk/internal/api/server"
	"gitlab.com/indikator/oooztk/internal/config"
)

func main() {
	config.InitConfig()
	database.InitDB()
	server.RunServer()
}
