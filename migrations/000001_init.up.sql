CREATE TABLE oooztk.votes (
	vote_id VARCHAR(36) NOT NULL,
	voting_id varchar(36) NOT NULL,
	option_id varchar(36) NOT NULL
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;
ALTER TABLE oooztk.votes ADD CONSTRAINT votes_UN UNIQUE KEY (vote_id,voting_id,option_id);